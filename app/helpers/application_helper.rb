# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  def is_admin?
      current_user.has_role?(:admin)
  end

  def parent_layout(layout)
    @view_flow.set(:layout, output_buffer)
    output = render(:file => "layouts/#{layout}")
    self.output_buffer = ActionView::OutputBuffer.new(output)
  end

  def homepage_walkthrough_counter(section)
    if @walk_through_counter.nil?
      -1
    else
      @walk_through_counter[section.to_sym]
    end
  end

  def sp_progress_to_percent(value)
    value = value.to_f
    value ||= 0
    ((value / 20.to_f) * 100).round
  end

end
