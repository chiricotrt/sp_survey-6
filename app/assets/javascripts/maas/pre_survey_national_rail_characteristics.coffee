## MAIN CALL ##
jQuery ->
  setDependencies()
  setAnswers()

## SETTING DEPENDENCIES ##
setDependencies = ->
  # Dependencies
  $("#pre_survey_national_rail_characteristic_frequency_of_usage").change ->
    if $(this).val() != "1"
      $("#national_rail_user_question").show()
    else
      $("#national_rail_user_question").hide()

  $("#pre_survey_national_rail_characteristic_pass_membership_1").click ->
    $("#railcard_user_questions").show()

  $("#pre_survey_national_rail_characteristic_pass_membership_0").click ->
    $("#railcard_user_questions").hide()

## SETTING ANSWERS ##
setAnswers = ->
  # pass_membership
  if $("#pre_survey_national_rail_characteristic_frequency_of_usage").val() != "1"
    $("#national_rail_user_question").show()

  # pass_period, pass_cost
  if $("#pre_survey_national_rail_characteristic_pass_membership_1").prop("checked")
    $("#railcard_user_questions").show()

