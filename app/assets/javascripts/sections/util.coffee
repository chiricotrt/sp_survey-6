util =
  hideDiv: (id) ->
    divId = "#d-#{id}"
    $(divId).addClass("hidden")
    $(divId).find("input,select").prop("required", false)

  showDiv: (id) ->
    divId = "#d-#{id}"
    $(divId).removeClass("hidden")
    $(divId).find("input[type!='checkbox'],select").prop("required", true)


window.util = util