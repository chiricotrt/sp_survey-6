require 'mail_officer'

class UsersController < ApplicationController
  layout 'smart'

  # before_filter :require_no_user, only: [:new, :create, :new_wsp]

  def new
    @user = User.new
  end

  def new_wsp
    @user = User.new
    @user.rn_id = params[:rn_id]
    @user.rn_study = params[:rn_study]
  end

  def update_wsp_email
    current_user.wsp_email = params[:wsp_email]
    if current_user.save
      render status: :ok
    else
      render status: :bad_request
    end
  end

  def sync_umove
    # Check if user already exists
    if params[:umove_id]
      @user = User.where(umove_id: params[:umove_id]).first
    end

    if @user.blank?
      @user = User.new_guest
      @user.city_id = params[:city_id]
      @user.umove_id = params[:umove_id]
      @user.user_type = "resident"
      @user.survey_email_start = params[:user_email] || ""

      # RN
      @user.rn_id = params[:rn_id]
      @user.rn_study = params[:rn_study]

      # Set browser
      browser = Browser.new(request.env['HTTP_USER_AGENT'], accept_language: "en-us")
      @user.browser = browser.name
      @user.ip_address = request.remote_ip
      if browser.device.mobile?
        @user.device = User::MOBILE
      elsif browser.device.tablet?
        @user.device = User::TABLET
      else
        @user.device = User::DESKTOP
      end

      @user.registration_source = 'web'
      @user.participating = true
      @user.save!(context: :human)
      @user.activate!
      @user.consent!
    end

    # Update MAN survey pre-screening answers #
    if params[:user_screen_responses]
      # Smartphone
      if params[:user_screen_responses][:smart_phone] and params[:user_screen_responses][:smart_phone] == "0"
        @user.screenout!
        redirect_to "http://endlinks.researchnow.com/d?status=2&rnid=#{@user.rn_id}&study=#{@user.rn_study}"
        return
      end

      # Age
      # 30%  18-30 years old
      # 35%  31-45 years old
      # 30%  46-60 years old
      # 5%   More than 61 years old
      if params[:user_screen_responses][:age]
        age = params[:user_screen_responses][:age].to_i
        # Set rn_age
        @user.rn_age = age
        @user.save

        if age < 18
          @user.screenout!
          redirect_to "http://endlinks.researchnow.com/d?status=2&rnid=#{@user.rn_id}&study=#{@user.rn_study}"
          return
        end

        # Check quota
        if User.check_quota_age(age)
          pre_survey_socio_economic_characteristic = PreSurveySocioEconomicCharacteristic.new(user_id: @user.id)
          pre_survey_socio_economic_characteristic.age = age
          pre_survey_socio_economic_characteristic.save(validate: false)
        else
          Rails.logger.debug "SURVEY-DEBUG: Overquota -> AGE"
          # 3.3
          @user.overquota!(3.3)
          redirect_to "http://endlinks.researchnow.com/d?status=3&rnid=#{@user.rn_id}&study=#{@user.rn_study}"
          return
        end
      end

      # Sex
      # 50/50 gender spilt
      if params[:user_screen_responses][:sex]
        sex = params[:user_screen_responses][:sex].to_i
        # Set rn_sex
        @user.rn_sex = sex
        @user.save

        # Check quota
        if User.check_quota_sex(sex)
          if @user.pre_survey_socio_economic_characteristic
            pre_survey_socio_economic_characteristic = @user.pre_survey_socio_economic_characteristic
          else
            pre_survey_socio_economic_characteristic = PreSurveySocioEconomicCharacteristic.new(user_id: @user.id)
          end

          pre_survey_socio_economic_characteristic.sex = sex
          pre_survey_socio_economic_characteristic.save(validate: false)
        else
          Rails.logger.debug "SURVEY-DEBUG: Overquota -> SEX"
          # 3.2
          @user.overquota!(3.2)
          redirect_to "http://endlinks.researchnow.com/d?status=3&rnid=#{@user.rn_id}&study=#{@user.rn_study}"
          return
        end
      end

      # Licences
      # At least 60% to have a driving license
      licences = [
        params[:user_screen_responses][:licence_car],
        params[:user_screen_responses][:licence_motorcycle],
        params[:user_screen_responses][:licence_none]
      ]
      licences.compact!
      if not licences.empty?
        # Set rn_licence
        if licences.include?("1") or licences.include?("2")
          @user.rn_licence = 1
        else
          @user.rn_licence = 0
        end
        @user.save

        # Check quota
        if User.check_quota_licence(@user.rn_licence)
          pre_survey_private_mobility_characteristic = PreSurveyPrivateMobilityCharacteristic.new(user_id: @user.id)
          pre_survey_private_mobility_characteristic.driving_licences = licences.to_s
          pre_survey_private_mobility_characteristic.save(validate: false)
        else
          Rails.logger.debug "SURVEY-DEBUG: Overquota -> LICENCE"
          # 3.1
          @user.overquota!(3.1)
          redirect_to "http://endlinks.researchnow.com/d?status=3&rnid=#{@user.rn_id}&study=#{@user.rn_study}"
          return
        end
      end
    end

    # Update city_id (if needed)
    if params[:city_id] != @user.city_id
      @user.city_id = params[:city_id]
      @user.save!(context: :human)
    end

    # Autmatically login the user and redirect to overview
    # @user_session = UserSession.new(params[:user])
    @user_session = UserSession.new(@user)
    if @user_session.save!
      session[:time_zone] = @user_session.record.time_zone

      locale = "en"
      if @user.city_id == City::BUDAPEST
        locale = "hu"
      end

      if @user.city_id == City::TURIN
        if params[:lux_language] == "2"
          locale = "de"
        elsif params[:lux_language] == "3"
          locale = "fr"
        end
      end

      sp_experiment = @user.sp_experiment || SpExperiment.create(user_id: @user.id)
      # redirect_to new_sp_experiment_activity_choices_path(sp_experiment_id: sp_experiment)
      redirect_to next_section_sections_path
    else
      redirect_to "http://195.251.134.191/mainmain.php"
    end
  end

  def create
    # @user = params[:user] ? User.new(params[:user]) : User.new_guest
    if params[:user]
      @user = User.new(params[:user])
    else
      @user = User.new_guest
      @user.rn_id = params[:rn_id]
      @user.rn_study = params[:rn_study]
      @user.city_id = params[:city_id]
      @user.umove_id = params[:umove_id]
    end

    # Set browser
    browser = Browser.new(request.env['HTTP_USER_AGENT'], accept_language: "en-us")
    @user.browser = browser.name

    @user.registration_source = 'web'
    @user.participating = true
    if @user.save!(context: :human)
      @user.activate!
      @user.consent!

      # Autmatically login the user and redirect to overview
      # @user_session = UserSession.new(params[:user])
      @user_session = UserSession.new(@user)
      if @user_session.save!
        session[:time_zone] = @user_session.record.time_zone
        # redirect_to new_sp_experiment_url
        locale = "en"
        if @user.city_id == City::BUDAPEST
          locale = "hu"
        end

        redirect_to pre_questionnaire_url(locale: locale)
        # redirect_to pages_disclaimer_url
      else
        redirect_to root_url
      end
    else
      flash[:notice] = I18n.t('account.create_unsuccessful')
      flash[:class] = "content_error"

      if params[:wsp]
        render :action => 'new_wsp', :id => params[:user][:panelist_secure_id]
      else
        render :action => 'new'
      end
    end
  end

  def update
    @user = current_user
    if @user.update_attributes(params[:user])
      flash[:notice] = I18n.t('profile.update_successful')
      flash[:class] = "content_feedback"
      redirect_to pages_dashboard_path
    else
      render :action => 'edit'
    end
  end

  def update_password
    if current_user.valid_password? params[:user][:current_password]
      current_user.password = params[:user][:password]
      current_user.password_confirmation = params[:user][:password_confirmation]
      if current_user.save
        flash[:passwdupdated] = I18n.t('profile.passwd_successfully_changed')
        flash[:class] = "content_feedback"
        redirect_to pages_dashboard_path, notice: 'Your password has been updated'
      else
        flash[:passwdupdated] = I18n.t('profile.error_updating_password')
        flash[:class] = "content_error"
        redirect_to pages_dashboard_path
      end
    else
      flash[:passwdupdated] = I18n.t('profile.current_passwd_invalid')
      flash[:class] = "content_error"
      redirect_to pages_dashboard_path
    end
  end

  def screenout
    @user = User.find(params[:id])
    @user.screenout!

    redirect_to "http://endlinks.researchnow.com/d?rnid=#{@user.rn_id}&study=#{@user.rn_study}&status=2"
  end

  def submitIgnorePopup
    current_user.updatePromptProperty(params["dialogname"])
    render :nothing => true
  end

  def acknowledge_warning
    current_user.acknowledge_warning
    render :nothing => true
  end

  def dismiss_timezone_warnings
    dismiss = current_user.user_property.where(name: "dismiss_timezone_warnings").first_or_initialize

    if !dismiss.value.nil?
      dismiss.value = (dismiss.value == "0" ? "1" : "0")
    else
      dismiss.value = "1"
    end
    dismiss.save

    redirect_to pages_home_url
  end

end
