class AddDeviceFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :device_model, :string
    add_column :users, :device_os_version, :string
  end
end
