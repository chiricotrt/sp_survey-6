class AddNumberOfHoursOfCarSharingToSharedMobilitySection < ActiveRecord::Migration
  def change
    add_column :pre_survey_shared_mobility_characteristics, :number_of_hours_of_car_sharing, :integer
  end
end
