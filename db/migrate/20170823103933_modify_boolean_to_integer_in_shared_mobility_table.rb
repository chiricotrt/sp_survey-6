class ModifyBooleanToIntegerInSharedMobilityTable < ActiveRecord::Migration
  def change
    remove_column :pre_survey_shared_mobility_characteristics, :aware_of_car_club
    remove_column :pre_survey_shared_mobility_characteristics, :member_of_car_club
    add_column :pre_survey_shared_mobility_characteristics, :aware_of_car_club, :integer
    add_column :pre_survey_shared_mobility_characteristics, :member_of_car_club, :integer
  end
end
