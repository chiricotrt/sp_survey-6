# frozen_string_literal: true

desc 'Converts SP experiment data to CSV format'
task locale_datetimes: :environment do
  start_date = Date.new(2021,6,22)
  headers = %w[datetime locale completed]

  CSV.open('tmp/locale_datetimes.csv', 'wb') do |csv|
    csv << headers

    SpExperiment.where("created_at >= ?", start_date).each do |experiment|
      responses = experiment.responses
      next if responses.nil? || responses['introduction'].blank?

      row = [
        experiment.created_at.strftime('%d/%m/%Y %H:%M'),
        experiment.locale,
        responses['rp_survey'].present? ? 1 : 0
      ]

      csv << row
    end
  end
end

