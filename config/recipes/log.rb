set_default(:rotation_period) { "7" } # Rotation period (in days)
set_default(:max_size) { "5M" } # Max log file size

namespace :log do
  namespace :tail do
    desc "Tail all application log files on base folder"
    task :all, :roles => :app do
      run "tail -f #{shared_path}/log/*.log" do |channel, stream, data|
        puts "#{channel[:host]}: #{data}"
        break if stream == :err
      end
    end

    # desc "Tail all application log files on raw_data folder"
    # task :raw_data, :roles => :app do
    #   run "tail -f #{shared_path}/log/raw_data/*.txt" do |channel, stream, data|
    #     puts "#{channel[:host]}: #{data}"
    #     break if stream == :err
    #   end
    # end
  end

  desc "Install log rotation script; optional args: days=7, size=5M, group (defaults to same value as :user)"
  task :rotate, :roles => :app do
        template "logrotate.erb", "#{shared_path}/logrotate_script"
        run "#{sudo} cp #{shared_path}/logrotate_script /etc/logrotate.d/#{application}"
        run "#{sudo} rm #{shared_path}/logrotate_script"
  end

  # task :setup, :except => { :no_release => true } do
  #   run "mkdir -p #{shared_path}/log/stops_log"
  #   run "mkdir -p #{shared_path}/log/workers/detectstops"
  #   run "mkdir -p #{shared_path}/log/raw_data"
  #   run "mkdir -p #{shared_path}/log/MR_hook/logs"
  #   run "mkdir -p #{shared_path}/log/MR_hook/mutexes"
  #   run "mkdir -p #{shared_path}/log/user_classifier"
  #   run "mkdir -p #{shared_path}/log/learning_log"
  # end

  after "deploy:setup", "log:setup"
end
